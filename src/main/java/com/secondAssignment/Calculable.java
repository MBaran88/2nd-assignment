package com.secondAssignment;

/**
 * Created by RENT on 2017-05-18.
 */
public interface Calculable {
    int add(int a, int b);
    int substract(int a, int b);
    int multiply(int a, int b);
    int divide(int a, int b);
}
